resource "aws_db_instance" "pristine_poc_db" {
  allocated_storage      = var.db_storage
  engine                 = "mysql"
  engine_version         = var.db_engine_version
  instance_class         = var.db_instance_class
  db_name                = var.db_name
  username               = var.dbuser
  password               = var.dbpassword
  db_subnet_group_name   = var.db_subnet_group_name
  identifier             = var.db_identifier
  skip_final_snapshot    = var.skip_db_snapshot
  vpc_security_group_ids = [var.rds_sg]

  tags = {
    Terraform     = "true"
    Name          = "pristine-poc-by-Rahul Mishra"
    Project       = "Pristine"
    Organization  = "Kellton"
    Environment   = "POC"
    Approved_By   = "Suraj Kumar"
    Expiration_by = "Jan/2023"
  }
}