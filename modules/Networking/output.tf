
output "rds_sg" {
  value = aws_security_group.pristine_poc_rds_sg.id
}

output "db_subnet_group_name" {
  value = aws_db_subnet_group.pristine_poc_rds_subnetgroup.*.name
}