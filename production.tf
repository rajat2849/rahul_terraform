resource "random_id" "random_id_prefix" {
  byte_length = 2
}

locals {
  # production_availability_zones = ["${var.region}a", "${var.region}b", "${var.region}c"]
  cwd           = reverse(split("/", path.cwd))
  instance_type = local.cwd[1]
  location      = local.cwd[2]
  environment   = local.cwd[3]
  vpc_cidr      = "10.0.0.0/16"
}

module "Networking" {
  source               = "./modules/Networking"
  vpc_cidr             = local.vpc_cidr
  # access_ip            = var.access_ip
  public_sn_count      = 4
  private_sn_count     = 2
  db_subnet_group      = true
  availabilityzone     = ["us-east-1a" , "us-east-1b"]
  azs                  = 2
}

module "S3" {
  source = "./modules/S3"
  environment = var.environment
}

module "Database" {
  source               = "./modules/Database"
  db_storage           = 8
  db_engine_version    = "8.0"
  db_instance_class    = "db.t2.micro"
  db_name              = var.db_name
  dbuser               = var.dbuser
  dbpassword           = var.dbpassword
  db_identifier        = "pristine-poc-db"
  skip_db_snapshot     = true
  rds_sg               = module.Networking.rds_sg
  db_subnet_group_name = module.Networking.db_subnet_group_name[0]
}


