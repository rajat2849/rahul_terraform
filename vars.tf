
variable "region" {
}

variable "environment" {
  description = "Deployment Environment"
}



variable "db_name" {
  type = string
}

variable "dbuser" {
  type      = string
  sensitive = true
}

variable "dbpassword" {
  type      = string
  sensitive = true
}